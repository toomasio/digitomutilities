using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomUtilities
{
    public static class ObjectArrayUtilities
    {
        public static T[] RemoveNull<T>(this T[] array) where T : Object
        {
            int index = 0;
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] != null)
                    index++;
            }
            T[] sorted = new T[index];
            int sortedInd = 0;
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] != null)
                {
                    sorted[sortedInd] = array[i];
                    sortedInd++;
                }
                   
            }
            return sorted;
        }

        public static T[] SortByClosest<T>(this T[] array, Vector3 fromPosition) where T : Component
        {
            T[] sorted = new T[array.Length];
            int[] chosenInds = new int[array.Length];
            for (int i = 0; i < chosenInds.Length; i++)
                chosenInds[i] = -1;
            int chosenInd = 0;
            for (int i = 0; i < sorted.Length; i++)
            {
                float distance = Mathf.Infinity;
                int chosenIndTemp = -1;
                for (int j = 0; j < array.Length; j++)
                {
                    if (array[j] == null) continue;
                    bool match = false;
                    for (int k = 0; k < chosenInds.Length; k++)
                    {
                        if (j == chosenInds[k])
                            match = true;
                    }
                    if (match) continue;
                    Vector3 diff = array[j].transform.position - fromPosition;
                    float mag = diff.magnitude;
                    if (mag <= distance)
                    {
                        sorted[i] = array[j];
                        distance = mag;
                        chosenIndTemp = j;
                    }
                }
                chosenInds[chosenInd] = chosenIndTemp;
                chosenInd++;
            }
            return sorted;
        }

        public static T[] SortByFurthest<T>(this T[] array, Vector3 fromPosition) where T : Component
        {
            T[] sorted = new T[array.Length];
            int[] chosenInds = new int[array.Length];
            for (int i = 0; i < chosenInds.Length; i++)
                chosenInds[i] = -1;
            int chosenInd = 0;
            for (int i = 0; i < sorted.Length; i++)
            {
                float distance = 0;
                int chosenIndTemp = -1;
                for (int j = 0; j < array.Length; j++)
                {
                    if (array[j] == null) continue;
                    bool match = false;
                    for (int k = 0; k < chosenInds.Length; k++)
                    {
                        if (j == chosenInds[k])
                            match = true;
                    }
                    if (match) continue;
                    Vector3 diff = array[j].transform.position - fromPosition;
                    float mag = diff.magnitude;
                    if (mag >= distance)
                    {
                        sorted[i] = array[j];
                        distance = mag;
                        chosenIndTemp = j;
                    }
                }
                chosenInds[chosenInd] = chosenIndTemp;
                chosenInd++;
            }
            return sorted;
        }
    }
}


