using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitomUtilities
{
    public struct Timer
    {
        public float Delta { get; private set; }
        public float Perc { get; private set; }
        public float TargetTime { get; private set; }

        public Timer(float time)
        {
            TargetTime = time;
            Delta = 0;
            Perc = 0;
        }

        public void Tick()
        {
            if (TargetTime == 0)
            {
                Perc = 1;
                return;
            }
            Delta += Time.deltaTime;
            if (Delta > TargetTime)
                Delta = TargetTime;
            Perc = Delta / TargetTime;
        }

        public void Reset()
        {
            Delta = 0;
            Perc = 0;
        }
    }
}


